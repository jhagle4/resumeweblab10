/**
 * Created by student on 4/12/17.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT a.first_name, a.last_name, r.resume_name, r.resume_id FROM resume r ' +
        'JOIN account a ON r.account_id = a.account_id ' +
        'ORDER BY a.first_name;';
    console.log(query);

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'CALL account_getinfo(?)';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insertResume = function(params, callback) {
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?)';
    var queryData = [params.account_id, params.resume_name];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.insertSchool = function(resume_id, school_id, callback) {
    var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES (?)';
    var queryData = [resume_id, school_id];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.insertCompany = function(resume_id, company_id, callback) {
    var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES (?)';
    var queryData = [resume_id, company_id];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.insertSkill = function(skill_id, resume_id, callback) {
    var query = 'INSERT INTO resume_skill (skill_id, resume_id) VALUES (?)';
    var queryData = [skill_id, resume_id];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(resume_id, callback) {
    var query = 'DELETE FROM resume WHERE resume_id = ?';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.update = function(params, callback) {
    var query = 'UPDATE resume SET resume_name = ? WHERE resume_id = ?';
    var queryData = [params.resume_name, params.resume_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(resume_id, callback) {
    var query = 'CALL resume_id_getinfo(?)';
    var queryData = [resume_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};