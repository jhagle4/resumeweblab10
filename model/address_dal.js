var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT * FROM address where address_id = ?';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};



exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO address (street, zip_code) VALUES (?)';

    var queryData = [params.street, params.zip_code];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address where address_id = ?';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

/*
 //declare the function so it can be used locally
 var companyAddressInsert = function(company_id, addressIdArray, callback){
 // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
 var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

 // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
 var companyAddressData = [];
 for(var i=0; i < addressIdArray.length; i++) {
 companyAddressData.push([company_id, addressIdArray[i]]);
 }
 connection.query(query, [companyAddressData], function(err, result){
 callback(err, result);
 });
 };

 //export the same function so it can be used by external callers
 module.exports.companyAddressInsert = companyAddressInsert;

 //declare the function so it can be used locally
 var companyAddressDeleteAll = function(company_id, callback){
 var query = 'DELETE FROM company_address WHERE company_id = ?';
 var queryData = [company_id];

 connection.query(query, queryData, function(err, result) {
 callback(err, result);
 });
 };
 //export the same function so it can be used by external callers
 module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

 */
exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?,  zip_code= ? WHERE address_id = ?';
    var queryData = [params.street, params.zip_code, params.address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS account_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_getinfo (_account_id int)
 BEGIN

 SELECT * FROM account WHERE account_id = _account_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL account_getinfo (4);

 */

exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
