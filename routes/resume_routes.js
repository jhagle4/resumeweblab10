/**
 * Created by student on 5/9/17.
 */
var express = require('express');
var router = express.Router();
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');

router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result': result });
        }
    });
});

router.get('/', function(req, res){
    if(req.query.resume_id == null) {
        res.send('resume_id is null');
    }
    else {
        resume_dal.getById(req.query.resume_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('resume/resumeViewById', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    resume_dal.getById(req.query.account_id, function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAdd', {'account_id': req.query.account_id, 'school_name': result[1], 'company_name': result[2], 'skill_name': result[3] });
        }
    });
});

router.post('/insert', function(req, res) {
    if (req.body.school_id == null) {
        res.send('school_id must be provided.');
    }
    else if (req.body.company_id == null) {
        res.send('company_id must be provided.');
    }
    else if (req.body.skill_id == null) {
        res.send('skill_id must be provided.');
    }
    else {
        resume_dal.insertResume(req.body, function(err,result) {
            var resume_id = result.insertId;
            resume_dal.insertSchool(resume_id, req.body.school_id, function(err,result) {
                resume_dal.insertCompany(resume_id, req.body.company_id, function(err,result) {
                    resume_dal.insertSkill(req.body.skill_id, resume_id, function(err,result) {
                    });
                });
            });

            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                resume_dal.edit(resume_id, function (err, result) {
                    res.render('resume/resumeUpdate', {resume: result[0][0], was_successful: true});
                });
            }
        });
    };
});

router.get('/edit', function(req, res){
    if(req.query.resume_id == null) {
        res.send('A resume id is required');
    }
    else {
        resume_dal.edit(req.query.resume_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0][0]});
        });
    }

});

router.get('/update', function(req, res) {
    resume_dal.update(req.query, function(err, result){
        res.redirect(302, '/resume/all');
    });
});

router.get('/resumeSelectUser', function(req, res){
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeSelectUser', {'account': result});
        }
    });
});

module.exports = router;